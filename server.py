#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Clase (y programa principal) para un servidor de eco en UDP simple
"""

import socketserver
import sys
import simplertp
import secrets


class EchoHandler(socketserver.DatagramRequestHandler):
    """
    Echo server class
    """

    """ The server """
    def handle(self):
        # Escribe dirección y puerto del cliente (de tupla client_address)
        line = self.rfile.read()
        line_decode = line.decode('utf-8')
        print("El cliente nos manda " + line_decode)
        method = line_decode.split(" ")[0]
        if not line_decode.split(" ")[1].startswith("sip:"):
            answer = b"SIP/2.0 400 Bad Request\r\n\r\n"
            self.wfile.write(answer)
        elif not line_decode.split(" ")[2].startswith("SIP/2.0"):
            answer = b"SIP/2.0 400 Bad Request\r\n\r\n"
            self.wfile.write(answer)
        else:
            if method == "INVITE":
                answer_1 = b"SIP/2.0 100 Trying\r\n\r\n"
                answer_2 = b"SIP/2.0 180 Ringing\r\n\r\n"
                answer_3 = b"SIP/2.0 200 OK\r\n\r\n"
                answer = answer_1 + answer_2 + answer_3
                self.wfile.write(answer)
                send_audio(line_decode)

            if method == "BYE":
                answer = b"SIP/2.0 200 OK\r\n\r\n"
                self.wfile.write(answer)

            if method not in ['INVITE', 'BYE', 'ACK']:
                answer = b"SIP/2.0 405 Method Not Allowed\r\n\r\n"
                self.wfile.write(answer)


""" send the message RTP, send the audio"""


def send_audio(line_decode):
    audio_file = sys.argv[3]
    bit = secrets.randbelow(1)
    """ reciver informacion """
    reciver_info_split = line_decode.split('\r\n')
    ip = reciver_info_split[4].split(" ")[1]
    port = int(reciver_info_split[7].split(" ")[1])

    RTP_header = simplertp.RtpHeader()
    RTP_header.set_header(version=2, marker=bit, payload_type=14, ssrc=200002)
    audio = simplertp.RtpPayloadMp3(audio_file)
    simplertp.send_rtp_packet(RTP_header, audio, ip, port)

if __name__ == "__main__":
    # Creamos servidor de eco y escuchamos
    """ read arguments """
    try:
        if len(sys.argv) > 4:
            sys.exit("Usage: python3 server.py IP port audio_file")
        else:
            server_ip = sys.argv[1]
            server_port = int(sys.argv[2])
    except (ValueError, IndexError, FileNotFoundError):
        sys.exit("Usage: python3 server.py IP port audio_file")

    serv = socketserver.UDPServer((server_ip, server_port), EchoHandler)
    print("Listening...")

    try:
        serv.serve_forever()
    except KeyboardInterrupt:
        print("Finalizado servidor")

#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Programa cliente que abre un socket a un servidor
"""

import socket
import sys

""" read arguments """
method = sys.argv[1]
try:
    if method == "INVITE" or method == "BYE":
        if len(sys.argv) > 3:
            sys.exit("Usage: python3 client.py method receiver@IP:SIPport")
        else:
            reciver_info = sys.argv[2]
    else:
        sys.exit("Usage: python3 client.py method receiver@IP:SIPport")
except (ValueError, IndexError, FileNotFoundError):
    sys.exit("Usage: python3 client.py method receiver@IP:SIPport")

""" reciver informacion """
reciver_info_split = reciver_info.split("@")
reciver_name = reciver_info_split[0]
reciver_IP = reciver_info_split[1].split(":")[0]
reciver_SIPport = int(reciver_info_split[1].split(":")[1])

""" body of invite message """


def invite_msg(method, reciver_IP, reciver_name):
    return (method + " sip:" + reciver_name + "@" + reciver_IP +
            " SIP/2.0\r\n" + "Content-Length: 66\r\n\r\n" "v=0\r\n" +
            "o=robin@gotham.com 127.0.0.1\r\n" + "s=misesion\r\n" +
            "t=0\r\n" + "m=audio 34543 RTP\r\n")

# Creamos el socket, lo configuramos y lo atamos a un servidor/puerto
with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
    my_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    my_socket.connect((reciver_IP, reciver_SIPport))

    if method == "INVITE":
        message = invite_msg(method, reciver_IP, reciver_name)
    else:
        message = method + " sip:" + reciver_name + " SIP/2.0\r\n\r\n"
    print("Enviando: " + message)
    my_socket.send(bytes(message, 'utf-8') + b'\r\n')
    data = my_socket.recv(1024)
    if method == "INVITE":
        answer_1 = "SIP/2.0 100 Trying\r\n\r\n"
        answer_2 = "SIP/2.0 180 Ringing\r\n\r\n"
        answer_3 = "SIP/2.0 200 OK\r\n\r\n"
        if data.decode('utf-8') == answer_1 + answer_2 + answer_3:
            message = "ACK" + " sip:" + reciver_name + " SIP/2.0\r\n\r\n"
            print("Enviando: " + message)
            my_socket.send(bytes(message, 'utf-8') + b'\r\n')
    print('Recibido -- ', data.decode('utf-8'))
    print("Terminando socket...")

print("Fin.")
